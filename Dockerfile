FROM node:latest

#Create work dir
WORKDIR /usr/src/app

#install appdependencies
#wildcard used to ensure both package.json and packafe-lock.json are copied
COPY package*.json ./

RUN npm install
#If building code for production
#RUN npm install --only=production

#Bundle appsource
COPY . .

EXPOSE 5000
CMD ["node","webserver"]


