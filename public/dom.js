
function listnodes(node){
  for(let child of node.childNodes){
    if(child.nodeType == Node.ELEMENT_NODE){
      console.log(child);
    }
    }
  }
function getElement(node, tag){
  //let arr = Array.from(node.getElementsByTagName(tag));
  let arr = node.getElementsByTagName(tag)[0];
  console.log(arr.innerHTML);
}
function replaceElement(node, tag){
  let paragraphs = node.getElementsByTagName(tag);
  node.insertBefore(paragraphs[1], paragraphs[0]);

}
function talksAbout(node, string) {
  if(node.nodeType == Node.ELEMENT_NODE){
    for(let child of node.childNodes){
      if(talksAbout(child, string)){
        return true;
      }
    }
    return false;
  }
  else if(node.nodeType == Node.TEXT_NODE){          
    return node.nodeValue.indexOf(string) > -1;
  }    
}

function replaceImages(){
  let images = document.body.getElementsByTagName("img");
  for(let i = images.length -1; i >= 0; i--){
    let img = images[i];
    if(img.alt){
      let text = document.createTextNode(img.alt);
      console.log(img.parentNode);
      img.parentNode.replaceChild(text, img);
    }
  }
}
 function elt(type, ...children){
   let node = document.createElement(type);
   for(let child of children){
     if(typeof(child) != "string")node.appendChild(child)
     else node.appendChild(document.createTextNode(child));
   }
   return node;
 }

 document.getElementById("quote").appendChild(elt("footer","-", elt("strong","This is added using the elt function"),", preface to the edition of ",elt("em", "The best of me"),", 1950"))
 let gettag = document.getElementsByTagName("blockquote");

 let paras = document.getElementsByTagName("p");
 console.log(paras);
 for(let para of Array.from(paras)){
   console.log("Client height: ", para.clientHeight);
   console.log("Client width: ", para.clientWidth);
   console.log("Bounding rect: ", para.getBoundingClientRect())
   if(para.getAttribute("data-classified") == "secret") para.remove()
 }


//let bat = document.getElementById("bat");
//console.log(bat.src);

//replaceElement(document.body, "p");
//getElement(document.body, "p");
//console.log(document.getElementsByTagName("p").length);
//listnodes(document.body);
//console.log(talksAbout(document.body, 'book'));